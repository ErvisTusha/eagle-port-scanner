#!/usr/bin/env python
import socket
import threading
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--hostname", help="HostName or IP")
parser.add_argument("-p", "--port", help="Single Port or Range Start-End ")
parser.add_argument("-t", "--TimeOut", help="TimeOut MilliSeconds ",type=int)
args = parser.parse_args()
Host =args.hostname
Port = args.port
TimeOut=int(args.TimeOut)

open_ports = []
closed_ports = []
threads = []


def PortScan(Host,port,TimeOut=5000):
    TimeOut=TimeOut/1000
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.settimeout(TimeOut)
    con = s.connect_ex((Host,port))
    if con == 0:
        open_ports.append(port)
        #print(s.recv(256))
    else:
        closed_ports.append(port)
    s.close()

def CheckHost(Host):
    try:
        socket.gethostbyname(Host)
    except :
        # this means could not resolve the host
        print( "There was an error resolving the host")
        exit()

def ValidPort(Port):
    try:
        if int(Port)<=65535:
            return True
        else:
            return False
    except :
        return False

def rangeScan(Port):
    range = Port.split("-")
    try:
        StartPort = int(range[0])
        EndPort =int(range[1])
    except:
        return False
    if ValidPort(StartPort) and ValidPort(EndPort) and StartPort<EndPort:
        return StartPort,EndPort
    else:
        return False

def rangePortScan(Host,StartPort,EndPort,TimeOut):
        EndPort=EndPort+1
        for i in range(StartPort,EndPort):
            t= threading.Thread(target=PortScan,args=(Host,i,TimeOut))
            threads.append(t)
        for i in range(len(threads)-1):
            threads[i].start()
        for i in range(len(threads)-1):
            threads[i].join() 

def ScanHost(Host,Port,TimeOut=5000):
    if ValidPort(Port):
        PortScan(Host,int(Port),TimeOut)
    elif rangeScan(Port):
        StartPort,EndPort=rangeScan(Port)
        rangePortScan(Host,StartPort,EndPort,TimeOut)
    elif Port =='all':
        rangePortScan(Host,0,65535,TimeOut)
    else:
        print("wrong format")

CheckHost(Host)   
ScanHost(Host,Port,TimeOut)
print(open_ports)

'''
TODO FIXME Commenting CODE 
FIXME P1 threading 
FIXME P2 Run only with python 3 and up

TODO Scan most common  port 
TODO P2 TCP,UDP , SYN ,ACT etc 
TODO P3 Identify  Services
'''